<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>TO Do List Ajax!</title>

    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <script src="jquery-1.12.3.min.js"></script>


    <script type="text/javascript">

    function cargarTareaEnFormulario (t) {
	    var taskForm = $( "#taskForm" ).get(0);
	    taskForm.taskTitleInput.value = t.titulo;
	    taskForm.taskId.value = t.id;
	    taskForm.taskDateInput.value = t.fecha;
	    taskForm.taskDetails.value = t.descripcion;
    }

    function cargarListadoTareas () {
	    // Borrar los elementos hijos, para cuando se refresque la lista
	    $( "#todoList" ).empty();

	    $.get( "tareas.php"
		    , {}
		    , function (data, status, xhr) {
			    console.log(data);
			    $.each(data, function (i, tarea) {
				    tarea.fecha = tarea.fecha.replace(/ .*/g, '');
				    $( "#todoList" ).append($("<li>")
					    .append($( "<a>" )
						    .attr('href', '#')
						    .text(tarea.titulo + " - " + tarea.fecha)
						    .click((function (t) { return function () {
 							    cargarTareaEnFormulario(t);
 						    }} (tarea)))));
			    })
				    
		    }
		    , "json");
    }
    
    $( document ).ready(function () {
	    cargarListadoTareas();
    });

    function ajaxSubmitForm () {
	    /*
	    $.ajax({
		    type: "POST",
		    url: "guardarTarea.php",
		    data: $( "#taskForm" ).serialize(),
		    success: function(data) { alert('hlola'); cargarListadoTareas();}, 
		    error: function(xhr, textStatus, errorThrown) { alert('2fooasod'); }
	    });
	     */
	    
	    $.post("guardarTarea.php", $( "#taskForm" ).serialize(), function (data) {
		    cargarListadoTareas();
		    $( "#taskForm" )[0].reset();
	    });
    }

    function ajaxBorrarTarea () {
	    $.post("borrarTarea.php", $( "#taskId" ).serialize(), function (data) { cargarListadoTareas(); $( "#taskForm" )[0].reset();});
    }    
    </script>

  </head>
  <body>
    <div class="container-fluid">
      <div class="row">
	<div class="col-md-4">
	  <h3 class="text-center">
	    Listado de Tareas
	  </h3>
	  <button id="newTaskBtn" type="button" class="btn btn-default" onclick="location.href='index.php'">
	    Nueva Tarea
	  </button>

	  <ul id="todoList">	    
	  </ul>

	</div>
	<div class="col-md-8">
	  <h3 class="text-center">
	    Datos de Tarea
	  </h3>
	  <form role="form" id="taskForm" action="#">
	    <div class="form-group">
	      <input type="hidden" id="taskId" name="taskId"/>
	      <label for="taskTitleinput">
		Título
	      </label>
	      <input type="text" class="form-control" name="taskTitleInput" "id="taskTitleInput" placeholder="Título de la tarea...">
	    </div>

	    <div class="form-group">
	      <label for="taskDateInput">
		Fecha
	      </label>
	      <input type="date" class="form-control" id="taskDateInput" name="taskDateInput"/> 
	    </div>

	    <div class="form-group">
	      <label for="taskDetails">
		Detalles
	      </label>
	      <textarea name="taskDetails" form="taskForm" class="form-control" placeholder="Descripción detallada de la tarea..."></textarea>
	    </div>

	    <button type="button" class="btn btn-primary"onclick="ajaxSubmitForm();">
	      Guardar
	    </button>
	    
	    <button type="reset" class="btn btn-danger">
	      Limpiar
	    </button>	    
	    <button id="deleteTaskBtn" type="button" class="btn btn-danger" onclick="ajaxBorrarTarea();">
	      Borrar
	    </button>
	  </form>
	</div>
      </div>
    </div>   
  </body>
</html>
